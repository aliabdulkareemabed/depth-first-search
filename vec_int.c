#include "vec_int.h"


vec_int vec_int_create(size_t capacity)
{
    vec_int vec;
    if(capacity < 2) capacity = 2;
    int* mem_pool = (int*) malloc(sizeof(int) * capacity);
    if(mem_pool == NULL)
    {
        printf("init vector malloc failed...\n");
        vec.mem_pool = NULL;
        vec.size = 0;
        vec.capacity = 0;
        vec.state = vec_error;
        return vec;
    }
    vec.mem_pool = mem_pool;
    vec.size = 0;
    vec.capacity = capacity;
    vec.state = vec_init;
    return vec;
}


static void vec_int_grow(vec_int* vec)
{
    size_t new_capacity = (vec->capacity * 3) / 2;
    int* new_pool = (int*) malloc(sizeof(int) * new_capacity);
    if(new_pool == NULL)
    {
        printf("Growing int vec failed\n ");
        return;
    }
    for(size_t i = 0, n = vec->size; i < n; ++i)
    {
        new_pool[i] = vec->mem_pool[i];
    }
    free(vec->mem_pool);
    vec->mem_pool = new_pool;
    vec->capacity = new_capacity;

}
void vec_int_add(vec_int* vec, int elem)
{
    if(vec->size >= vec->capacity)
    {
        vec_int_grow(vec);
    }
    vec->mem_pool[vec->size] = elem;
    vec->size++;
}

void vec_int_print(vec_int* vec)
{
    for(size_t i = 0, n = vec->size; i < n; ++i)
    {
        printf("%d ", vec->mem_pool[i]);
    }
    printf("\n");
}

void vec_int_free(vec_int* vec)
{
    if(vec->state != vec_init) 
    {
        printf("vec int not initialized... \n");
        return;
    }
    free(vec->mem_pool);
    vec->size = vec->capacity = 0;
    vec->state = vec_free;
}
static void move_bytes(void* d,  void* s, size_t byte_count)
{
    unsigned char* dest = (unsigned char*) d;
    unsigned char* source = (unsigned char*) s;
    while(byte_count--) *dest++ = *source++ ;
}
void move_right(void* mem_begin, size_t index, size_t elem_size, void* mem_end)
{
    unsigned char* start = (unsigned char*)mem_begin + (index * elem_size);

    unsigned char* end =  mem_end;
    
    while(end != start )
    {
      move_bytes(end, end - elem_size, elem_size);
      end -= elem_size;
    }
}

void move_left(void* mem_begin, size_t index, size_t elem_size, void* mem_end)
{
    unsigned char* start = (unsigned char*)mem_begin + (index * elem_size);

    unsigned char* end =  mem_end;
    while(end != start)
    {
        move_bytes(start ,start + elem_size, elem_size);
        start += elem_size;
    }
    
}

void vec_int_insert(vec_int * vec, size_t index, int elem)
{
    if(vec->size >= vec->capacity)
    {
        vec_int_grow(vec);
    }
    move_right(vec->mem_pool, index, sizeof(int), & (vec->mem_pool[vec->size]) );
    vec->size++;
    vec->mem_pool[index] = elem;
    
}

void vec_int_delete(vec_int* vec, size_t index)
{
    if(vec->size == 0) 
    {
        printf("empty vec... returning\n");
        return;
    }
    move_left(vec->mem_pool, index, sizeof(int), &(vec->mem_pool[vec->size -1 ]));
    vec->size--;
}

void vec_int_set(vec_int* vec, int index, int elem)
{
    if(index >= (int)vec->size)
    {
        if(index >= (int)vec->capacity)
        {
            printf("this index is not reserved yet\n");
        }
        else
        {
            printf("this index is reserved but not yet added\n");
        }
        return;
        
    }

    else if(index < 0)
    {
        printf("passed a negative index\n");
        return;
    }
    else
    {
        vec->mem_pool[index] = elem;
    }

}
int vec_int_get(vec_int* vec, int index)
{
    if(index < 0) 
    {
        printf("passed a negative index\n");
        return -1;
    }
    else if(index >= (int)vec->size)
    {
        printf("this element is outside the boundary of access\n");
        return -1;
    }
    else
    {
        return vec->mem_pool[index];
    }

}

vec_int vec_int_view(vec_int vec, size_t start_view_index, size_t size)
{
    vec.mem_pool = vec.mem_pool + start_view_index;
    vec.size = size;
    return vec;
}



void vec_int_move(vec_int* dest, vec_int* src)
{
    *dest = *src;
    src->mem_pool = NULL;
    src->capacity = 0;
    src->size = 0;
    src->state = vec_free;
}

void vec_int_copy(vec_int* dest, vec_int* src)
{
    if(dest->state == vec_init)
    {
        vec_int_free(dest);
    }
    size_t size = src->size;
    
    *dest = vec_int_create(src->capacity);
    dest->size = size;

    int* s = src->mem_pool;
    int* d = dest->mem_pool;
    while(size--) *d++ = *s++ ;
    
    
}

