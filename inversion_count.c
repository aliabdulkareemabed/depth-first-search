#include "vec_int.h"


typedef struct InvVec
{
    vec_int vec;
    int count;

} InvVec;



InvVec merge_and_count_splits_inv(vec_int left, vec_int right) // same idea as merge sort merge function
{

    int invCount = 0;
    size_t n = left.size + right.size ;

    vec_int v = vec_int_create(n);

    size_t i,j,k;

    for(i = 0, j = 0; i < left.size && j < right.size; ) 
    {
        if( left.mem_pool[i] > right.mem_pool[j])
        {
            vec_int_add(&v,right.mem_pool[j++]);
            invCount += left.size - i ;
        }
        else
        {
            vec_int_add(&v,left.mem_pool[i++]);
        }
    }

    if(i == left.size)
    {
        while(j < right.size)
        {
            vec_int_add(&v,right.mem_pool[j++]);
        }
    }

    else 
    {
        while(i < left.size)
        {
            vec_int_add(&v,left.mem_pool[i++]);
        }

    }
    
    i = j = k = 0;
    while(i < left.size)
    {
        left.mem_pool[i++] = vec_int_get(&v,(int)k++);
    }
    while(j < right.size)
    {
        right.mem_pool[j++] = vec_int_get(&v,(int)k++);
    }
    vec_int_free(&v);

    InvVec inv = {vec_int_view(left, 0, left.size + right.size), invCount};
    return inv;
}

InvVec count_inv_and_sort(InvVec v) //same idea as merge_sort recursive function
{
    if(v.vec.size < 2) 
    {
        v.count = 0;
        return v;
    }
    InvVec left  = count_inv_and_sort((InvVec) {vec_int_view(v.vec, 0, v.vec.size/2), v.count});
    InvVec right = count_inv_and_sort((InvVec) {vec_int_view(v.vec, v.vec.size/2, v.vec.size - v.vec.size/2), v.count});
    InvVec splits= merge_and_count_splits_inv(left.vec, right.vec);


    return (InvVec){splits.vec, left.count + right.count + splits.count};
}

void count_inv_brute(vec_int* v) // just for testing purposes that we counted them correctly
{
    int count = 0;
    for(int i = 0, n1 = (int)(v->size - 1) ; i < n1; ++i)
    {
        for(int j = i + 1, n2 = (int)(v->size); j < n2 ; ++j)
        {
            if(vec_int_get(v, i) > vec_int_get(v,j))
            {
                ++count;
            }

        }
    }

    printf("brute force counted inv  = %d\n",count);
}



int main()
{
    vec_int vec = vec_int_create(4);

    vec_int_add(&vec,10);
    vec_int_add(&vec,15);
    vec_int_add(&vec,25);
    vec_int_add(&vec,-10);
    vec_int_add(&vec,-17);
    vec_int_add(&vec,-22);
    vec_int_add(&vec,-0);
    vec_int_add(&vec,11);
    


    

    vec_int_insert(&vec,vec.size,17);
    vec_int_insert(&vec,1,0);

    
    vec_int view = vec_int_view(vec,0,vec.size - 0);
    vec_int_print(&vec);
    vec_int_print(&view); 
    
    count_inv_brute(&vec);
    InvVec inv = {vec,0};
	inv = count_inv_and_sort(inv);

    printf("nlogn counted inversion =  %d\n",inv.count);


}
