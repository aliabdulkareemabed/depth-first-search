#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <string_view>
#include <iterator>
#include <algorithm>

using std::string;
using std::vector;
using std::cout;
using std::unordered_set;
using std::string_view;

typedef unsigned long ul;

//assuming all strings have the same number of characters
int Xedge;
int Yedge;

std::string_view match_word;

bool visited(vector<int>const& current_path , int const& index);
void search(vector <vector<char>> const& inboard, int const& start_index, bool& found);

void add_neighbours(vector<int>const&  current_path, vector<vector<int>>& paths, bool& found, vector <vector<char>> const& inboard)
{

  if(current_path.size() == match_word.size()) 
  {  
    found = true;
    paths.emplace_back(current_path);
    return;
  }
  
  int current_index = current_path.back();  
  char next_char = match_word[current_path.size()];
  
  int row = current_index / Xedge;
  int col = current_index % Xedge;

    //adding left
  if (col - 1 >= 0 && inboard[(ul)row][(ul)(col - 1)] == next_char && !visited(current_path, current_index - 1) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index - 1);
    paths.emplace_back(new_path);
  }

  //adding right
  if(col + 1 < Xedge && inboard[(ul)row][(ul)col + 1] == next_char && !visited(current_path, current_index  + 1) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index + 1);
    paths.emplace_back(new_path);
  }
  
  //adding top
  if(row - 1 >= 0 && inboard[(ul)(row - 1)][(ul)col] == next_char  && !visited(current_path, current_index -  Xedge) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index - Xedge);
    paths.emplace_back(new_path);
  }
  
  //adding buttom
  if(row + 1 < Yedge && inboard[(ul)row + 1][(ul)col] == next_char  && !visited(current_path, current_index + Xedge) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index + Xedge);
    paths.emplace_back(new_path);
  }
  
    //--------- adding diagonals ---------------//
    //top left
    if (col - 1 >= 0 && row - 1 >= 0 && inboard[(ul)(row - 1)][(ul)(col - 1)] == next_char && !visited(current_path, current_index - Xedge - 1) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index - Xedge - 1);
    paths.emplace_back(new_path);
  }

    //top right
    if (col + 1 < Xedge && row - 1 >= 0 && inboard[(ul)(row - 1)][(ul)col + 1] == next_char && !visited(current_path, current_index - Xedge + 1) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index - Xedge + 1);
    paths.emplace_back(new_path);
  }

    // buttom left
    if (col - 1 >= 0 && row + 1 < Yedge && inboard[(ul)row + 1][(ul)(col - 1)] == next_char && !visited(current_path, current_index + Xedge - 1) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index + Xedge - 1);
    paths.emplace_back(new_path);

  }

    //buttom right
    if (col + 1 < Xedge && row + 1 < Yedge && inboard[(ul)row + 1][(ul)col + 1] == next_char && !visited(current_path, current_index + Xedge + 1) ) 
  {
    auto new_path = current_path;
    new_path.emplace_back(current_index + Xedge + 1);
    paths.emplace_back(new_path);
  }
}

bool visited(vector<int> const& current_path, int const& index)
{
  
  if(std::find(current_path.begin(), current_path.end(), index) != current_path.end())
  {
    return true;
  }
  return false;
}

bool valid_string(vector <vector<char>> const& inboard, string const& input)
{
  
  bool found = false;
  Yedge = (int) inboard.size();
  Xedge = (int) inboard[0].size();

  match_word = input;
    
  for(int i = 0; i < Yedge; ++i)
  {
    for(int j = 0; j < Xedge; ++j)
    {
      if(inboard[(ul)i][(ul)j] == input[0]) //checking for first character matching
      {
        search(inboard, i * Xedge + j, found);
        if(found) {goto DONE;}
      }
    }
  }

    DONE:
    return found;
}

void search(vector <vector<char>> const& inboard, int const& start_index, bool& found)
{
  vector<int> start_path;
  start_path.emplace_back(start_index);

  vector<vector<int>> paths;
  paths.emplace_back(start_path);

  while((!paths.empty()) && !found)
  {
    auto top_path = paths.back();
    paths.pop_back();
    add_neighbours(top_path, paths, found, inboard);
  }
  if(found)
  {
      cout << "found at indices:\n";
      for(auto const& i: paths.back() )
      {
          std::cout <<i <<" ";
      }
  }   
}

int main()
{
    vector<vector<char>> inboard;
    inboard.push_back({'h','a','c','b','y'});
    inboard.push_back({'e',' ','o','z','y'});
    inboard.push_back({'l','l','w','l','r'});
    inboard.push_back({'h','o','d','o','l'});
    inboard.push_back({'k','v','e','b','l'});
    inboard.push_back({'k','a','w','h','q'});

    string word = "hello world";
    valid_string(inboard,word);
    return 0;
}