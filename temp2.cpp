

class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
     const int N = temperatures.size();    
        vector<int> sol(N,0);

        int* ss = (int*) alloca(sizeof(int) * (N-1));
        int* base = ss;
        for(long i = 0; i < N - 1;++i)
        {
            if(temperatures[i] < temperatures[i+1])
            {
                sol[i] = 1;
                 while(base != ss && temperatures[*(ss-1)] < temperatures[i+1])
                {
                    sol[*(ss-1)] = i + 1 - *(ss-1);

                    if( base != ss) --ss;
                }
            }
            else
            {
                *ss++ = i;
            }
        }    
        return sol;
    }       
};