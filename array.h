#ifndef ARRAY_H
#define ARRAY_H 
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct array_int
{
	int* elements;
	size_t size;

} array_int;
array_int array_int_create(size_t n);
int array_int_get(array_int* a, int index);

void array_int_free(array_int* a);
void array_int_set(array_int* a, int index, int elem);

array_int array_int_view(array_int a, int index );

#endif
