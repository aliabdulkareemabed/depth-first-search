#include <stdio.h>

typedef struct halves
{
	int first_half;
	int second_half;
	unsigned int first_half_pow;

} halves;

unsigned int num_power_calculate(int x)
{
	unsigned int i = 0;
	while( x != 0)
	{
		x /= 10;
		++i;
	}
	return i;
}

halves halves_compute(int n)
{
	unsigned int n_digit_count = num_power_calculate(n);

	halves h = {1,1,0,0};

	for (unsigned int i = 0; i < n_digit_count/2; ++i)
	{
		h.first_half *= 10;
	}

	h.second_half = n / h.first_half;
	h.first_half  = n % h.first_half;

	h.first_half_pow = n_digit_count / 2;

	return h;

}

unsigned int ten_power(unsigned int power)
{
	unsigned val = 1;
	while ((power > 0))
	{
		val *= 10;
		--power;
	}
	return val;
}

int katsubara(int x, int y)
{
	// decompose x into a , b (if x is 1234 -> a = 12 & b = 34) and same for y
	if(x < 10 || y < 10) return x * y;

	halves h1 = halves_compute(x);
	halves h2 = halves_compute(y);


	int ac = katsubara(h1.second_half, h2.second_half);
	int bd = katsubara(h1.first_half, h2.first_half);


	int ad = katsubara(h1.second_half, h2.first_half);
	int bc = katsubara(h1.first_half, h2.second_half);

	unsigned int ac_power = ten_power(h1.first_half_pow + h2.first_half_pow);
	
	unsigned int ad_power = ten_power(h1.first_half_pow);
	unsigned int bc_power = ten_power(h2.first_half_pow);

	return  ((int)ac_power * ac) + ((int)ad_power * ad)  +((int)bc_power * bc) +  bd;
	
}

int main()
{
	int x = katsubara(11,1111);
	printf("%d\n", x);
}

