#include <iostream>
#include <vector>
using namespace std;


    vector<int> dailyTemperatures(vector<int> temperatures) {
        
        vector<int> sol(temperatures.size(),0);
        
        for(int i = 0; i < temperatures.size(); ++i)
        {
            
            if(sol[i] == 0)
            {
                int prev = temperatures[i];
                int j = i + 1;
               
                while(j < temperatures.size())   
                {
     
                    if(temperatures[j] > prev)
                    {
                     sol[j - 1] = 1;   
                    }
                    
                    if(temperatures[j]> temperatures[i])
                    {
                        sol[i] = j-i;
                        break;
                    }
                    
                    prev = temperatures[j];
                    ++j;
                    
                }
                
            }
        }
        return sol;
    }
    int main()
    {
        auto sol = dailyTemperatures({73,74,75,71,69,72,76,73});

        for(auto ref : sol)
        {
            cout << ref << ' ';
        }








    }