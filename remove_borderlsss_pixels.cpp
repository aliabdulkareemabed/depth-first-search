#include <vector>
#include <unordered_set>
#include <iostream>
#include <cstdint>


typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef unsigned long ul;
typedef long long ll;

std::vector<int> get_boarder_ones (std::vector<int> const& matrix );
void expand_neighbours (std::vector <int> & paths, std::vector<bool> & visited, std::unordered_set<int>& keep,std::vector<int>const& pixels);
void print_vec(std::vector<int> v);
bool is_inside( int i, int j);

std::vector<int> input = 
{
    1,0,0,0,1,
    0,1,1,1,0,
    1,0,1,0,1,
    0,1,1,1,0,
    1,0,0,0,1,
};

#define rows 5
#define cols 5

void print_vec(std::vector<int> v)
{
    for(u64 i = 0; i < v.size(); ++i)
    {
        std::cout << " " << v[i] << " ";
        if( (i +1) % rows == 0)
        {
            std::cout << "\n";
        }
    }
}


bool is_inside( int i, int j)
{
    return ((i >= 0 && i < rows) && (j >= 0 && j < cols));
    
}

void expand_neighbours (std::vector <int> & paths, std::vector<bool> & visited, std::unordered_set<int>& keep, std::vector<int>const& pixels)
{
    int index = paths.back();
    paths.pop_back();
    int near[4] =  {index - 1, index + 1, index - cols, index + cols};

    for(auto const& c : near)
    {
        int i = c / rows;
        int j = c % rows;
        if(is_inside(i,j) && !visited[(ul)c] && pixels[(ul)c] == 1)
        {
            visited[(ul) c] = true;
            keep.insert(c);
            paths.push_back(c);
        }
    }
}

 std::unordered_set<int> search(std::vector<int> & pixels)
{
    auto paths = get_boarder_ones(pixels);
    auto keep = std::unordered_set<int>(paths.begin(),paths.end());
    
    std::vector<bool> visited (pixels.size(),false);

    
    while(!paths.empty())
    {
        expand_neighbours(paths,visited,keep, pixels);
    }
    return keep;
}



std::vector<int> sol (std::vector<int>&v)
{
    std::vector<bool> visited (v.size(),false);

    return v;
}

std::vector<int> get_boarder_ones (std::vector<int> const& matrix )
{
    std::vector<int> result;



    // first row except final elem and first elem
    for(int j = 0; j < cols ; ++j)
    {
        if (matrix[(ul) j] == 1)
        {
            result.push_back(j);
        }
    }

    //first col
    for (int i = 1; i < rows; ++i)
    {
        int index =  rows * i;
        if (matrix[ (ul) index] == 1)
        {
            result.push_back(index);
        }

    }

    //last col
    for (int i = 1; i < rows; ++i)
    {
        int index = ( rows * i) + (cols - 1 );
        if (matrix[static_cast<ul>(index)] == 1)
        {
            result.push_back(index);
        }
        
    }

    //last row except final elem and first elem
    for(int j = 1; j < cols - 1; ++j)
    {
        int index = (rows * (rows -1 )) + j;
        if (matrix[static_cast<ul>(index)] == 1)
        {
            result.push_back(index);
        }
    }

    return result;

}

int main()
{
    print_vec(input);

    auto keep = search(input);
    
    std::cout<<"------------- solution----------- \n";
    for(u64 i = 0; i < input.size() ;++i)
    {
        if(keep.find((int)i) == keep.end())
        {
            input[i] = 0;
        }
    }
    print_vec(input);
    return 0;
}
