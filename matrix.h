#ifndef MATRIX_DATA_H
#define MATRIX_DATA_H

#include "array.h"

typedef struct matrix
{
	array_int entries;

	int rows;
	int cols;

	// using union laters for i,j in the future

	int row_offset;
	int col_offset;

	int original_cols;

} matrix;


matrix matrix_create(int rows, int cols);
matrix matrix_get_view(matrix m, int start_x, int start_y, int rows, int cols);

int matrix_get_elem(matrix*m, int i, int j);
void matrix_free(matrix* m);

void matrix_set_elem(matrix*m ,int i, int j, int elem);
void matrix_print(matrix* m);
// void matrix_get_vec(void);

#endif

