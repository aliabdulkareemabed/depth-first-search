#include <stdio.h>

void exit(int status);

#define FALSE 0
#define TRUE 1

#define START_SQUARE 0
#define END_SQUARE 81


char board[9][9] = {
  "...7.....",
  "1........",
  "...43.2..",
  "........6",
  "...5.9...",
  "......418",
  "....81...",
  "..2....5.",
  ".4....3..",
};


void print_board(void)
{
	for(int row = 0; row<9;++row)
	{
		for(int col = 0; col<9; ++col)
		{
			printf("%c ",board[row][col]);
		}
		printf("\n");
	}
}


int valid (int row, int col, char p)
{
	/*----------- Checking a full column and a full row ------------*/
	for(int j = 0; j < 9; ++j)
	{
		if(board[row][j] == p) return FALSE;
	}
	for(int i = 0; i < 9; ++i)
	{
		if(board[i][col] == p) return FALSE;
	}


/*----------- Checking a square of 3 x 3 ------------*/
	row = 3 * (row/3);
	col = 3 * (col/3);

	for(int ri = 0; ri <3; ++ri)
	{
		for(int ci = 0; ci < 3; ++ci)
		{
			if(board[row + ri][col+ci] == p) return FALSE;
		}
	}

	return TRUE;

}

void solve(int current_square)
{
	if(current_square == END_SQUARE) /*We reached the end and it was solved*/
	{
		print_board();
		exit(0);
	}

	int row = current_square / 9;
	int col = current_square % 9;

	if(board[row][col] != '.') // basically, if the current square is already solved, go to the next one
	{
		solve(current_square + 1);
		return;
	}

	for(char p = '1'; p <= '9'; ++p)
	{
		if(!valid(row, col, p))
		{
			continue;
		}
		board[row][col] = p;
		solve(current_square + 1);

		board[row][col] = '.'; /*If we didn't solve it by expanding to current_square +1, we mark it as empty again*/
	}

}

int main()
{
	solve(START_SQUARE);
}
