#ifndef VEC_INT_H
#define VEC_INT_H
#include <stdlib.h>
#include <stdio.h>

typedef enum vec_state
{
    vec_error = 0X17,
    vec_init  = 0X19,
    vec_free  = 0X21,  

}vec_state;

typedef struct vec_int
{
    int* mem_pool;
    size_t capacity;
    size_t size;    
    vec_state state;
} vec_int;


/*---------- Interface Functions -----------*/

vec_int vec_int_create(size_t capacity);
vec_int vec_int_view(vec_int vec, size_t start_view_index, size_t size);

int vec_int_get(vec_int* vec, int element_index);

void vec_int_add(vec_int* vec, int elem);
void vec_int_print(vec_int* vec);
void vec_int_free(vec_int* vec);

void vec_int_insert(vec_int * vec, size_t index, int elem);
void vec_int_delete(vec_int * vec, size_t index);

void vec_int_set(vec_int* vec, int index, int elem);


#endif

