#include <stdio.h>
#include <stdlib.h>


typedef enum vec_state
{
    vec_error = 0,
    vec_init  = 1,
    vec_free  = 2,  

}vec_state;

typedef struct vec_int
{
    int* mem_pool;
    size_t capacity;
    size_t size;    
    vec_state state;
}vec_int;

vec_int vec_int_create(size_t capacity)
{
    vec_int vec;
    if(capacity < 2) capacity = 2;
    int* mem_pool = (int*) malloc(sizeof(int) * capacity);
    if(mem_pool == NULL)
    {
        printf("init vector malloc failed...\n");
        vec.mem_pool = NULL;
        vec.size = 0;
        vec.capacity = 0;
        vec.state = vec_error;
        return vec;
    }
    vec.mem_pool = mem_pool;
    vec.size = 0;
    vec.capacity = capacity;
    vec.state = vec_init;
    return vec;
}


static void vec_int_grow(vec_int* vec)
{
    size_t new_capacity = (vec->capacity * 3) / 2;
    int* new_pool = (int*) malloc(sizeof(int) * new_capacity);
    if(new_pool == NULL)
    {
        printf("Growing int vec failed\n ");
        return;
    }
    for(size_t i = 0, n = vec->size; i < n; ++i)
    {
        new_pool[i] = vec->mem_pool[i];
    }
    free(vec->mem_pool);
    vec->mem_pool = new_pool;
    vec->capacity = new_capacity;

}
void vec_int_add(vec_int* vec, int elem)
{
    if(vec->size >= vec->capacity)
    {
        vec_int_grow(vec);
    }
    vec->mem_pool[vec->size] = elem;
    vec->size++;
}

void vec_int_print(vec_int* vec)
{
    for(size_t i = 0, n = vec->size; i < n; ++i)
    {
        printf("%d ", vec->mem_pool[i]);
    }
    printf("\n");
}

void vec_int_free(vec_int* vec)
{
    if(vec->state != vec_init) 
    {
        printf("vec int not initialized... \n");
        return;
    }
    free(vec->mem_pool);
    vec->size = vec->capacity = 0;
    vec->state = vec_free;
}
static void move_bytes(void* d,  void* s, size_t byte_count)
{
    unsigned char* dest = (unsigned char*) d;
    unsigned char* source = (unsigned char*) s;
    while(byte_count--) *dest++ = *source++ ;
}
void move_right(void* mem_begin, size_t index, size_t elem_size, void* mem_end)
{
    unsigned char* start = (unsigned char*)mem_begin + (index * elem_size);

    unsigned char* end =  mem_end;
    
    while(end != start )
    {
      move_bytes(end, end - elem_size, elem_size);
      end -= elem_size;
    }
}

void move_left(void* mem_begin, size_t index, size_t elem_size, void* mem_end)
{
    unsigned char* start = (unsigned char*)mem_begin + (index * elem_size);

    unsigned char* end =  mem_end;
    while(end != start)
    {
        move_bytes(start ,start + elem_size, elem_size);
        start += elem_size;
    }
    
}

void vec_int_insert(vec_int * vec, size_t index, int elem)
{
    if(vec->size >= vec->capacity)
    {
        vec_int_grow(vec);
    }
    move_right(vec->mem_pool, index, sizeof(int), & (vec->mem_pool[vec->size]) );
    vec->size++;
    vec->mem_pool[index] = elem;
    
}

void vec_int_delete(vec_int* vec, size_t index)
{
    if(vec->size == 0) 
    {
        printf("empty vec... returning\n");
        return;
    }
    move_left(vec->mem_pool, index, sizeof(int), &(vec->mem_pool[vec->size -1 ]));
    vec->size--;
}

void vec_int_set(vec_int* vec, int index, int elem)
{
    if(index >= (int)vec->size)
    {
        if(index >= (int)vec->capacity)
        {
            printf("this index is not reserved yet\n");
        }
        else
        {
            printf("this index is reserved but not yet added\n");
        }
        return;
        
    }

    else if(index < 0)
    {
        printf("passed a negative index\n");
        return;
    }
    else
    {
        vec->mem_pool[index] = elem;
    }

}
int vec_int_get(vec_int* vec, int index)
{
    if(index < 0) 
    {
        printf("passed a negative index\n");
        return -1;
    }
    else if(index >= (int)vec->size)
    {
        printf("this element is outside the boundary of access\n");
        return -1;
    }
    else
    {
        return vec->mem_pool[index];
    }

}

vec_int vec_int_view(vec_int vec, size_t start_view_index, size_t size)
{
    vec.mem_pool = vec.mem_pool + start_view_index;
    vec.size = size;
    return vec;
}

vec_int merge(vec_int left, vec_int right)
{
    size_t n = left.size + right.size ;

    vec_int v = vec_int_create(n);

    size_t i,j,k;

    for(i = 0, j = 0; i < left.size && j < right.size; ) 
    {
        if( left.mem_pool[i] > right.mem_pool[j])
        {
            vec_int_add(&v,right.mem_pool[j++]);
        }
        else
        {
            vec_int_add(&v,left.mem_pool[i++]);
        }
    }

    if(i == left.size)
    {
        while(j < right.size)
        {
            vec_int_add(&v,right.mem_pool[j++]);
        }
    }

    else 
    {
        while(i < left.size)
        {
            vec_int_add(&v,left.mem_pool[i++]);
        }

    }
    
    i = j = k = 0;
    while(i < left.size)
    {
        left.mem_pool[i++] = v.mem_pool[k++];
    }
    while(j < right.size)
    {
        right.mem_pool[j++] = v.mem_pool[k++];
    }
    vec_int_free(&v);

    return vec_int_view(left, 0, left.size + right.size);
}

vec_int merge_sort(vec_int vec)
{
    if(vec.size < 2) return vec;
    vec_int left = merge_sort(vec_int_view(vec, 0, vec.size/2));
    vec_int right = merge_sort(vec_int_view(vec, vec.size/2, vec.size - (vec.size/2)));


    vec_int v = merge(left,right);
    vec_int_print(&v);
    return v;

}


int main()
{
    vec_int vec = vec_int_create(4);

    vec_int_add(&vec,10);
    vec_int_add(&vec,15);
    vec_int_add(&vec,25);
    vec_int_add(&vec,-10);
    vec_int_add(&vec,-17);
    vec_int_add(&vec,-22);
    vec_int_add(&vec,-0);
    
    
    
    vec_int_insert(&vec,vec.size,17);
    vec_int_insert(&vec,1,0);
    vec_int_insert(&vec, vec.size - 1, 12);
    vec = merge_sort(vec);
    vec_int_print(&vec);
    
    vec_int_free(&vec);

}
