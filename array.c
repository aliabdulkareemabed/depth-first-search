#include "array.h"

array_int array_int_create(size_t n)
{
	array_int a;
	if(n < 1) n = 1;
    int* mem_pool = (int*) malloc(sizeof(int) * n);
    if(mem_pool == NULL)
    {
        printf("init vector malloc failed...\n");
        a.elements = NULL;
        a.size = 0;
        return a;
    }

    a.elements = mem_pool;
    a.size = n;

    return a;
}


void array_int_free(array_int* a)
{
    free(a->elements);
    a->size = 0;
}

int array_int_get(array_int* a, int index)
{
    if(index < 0 || (size_t) index >= a->size)
    {
        printf("invalid index was passed in get\n");
        return -1;
    }

    return a->elements[index];
}

void array_int_set(array_int* a, int index, int elem)
{
    if(index < 0 || (size_t)index >= a->size)
    {
        printf("invalid index was passed in set\n");
        return;
    }

    a->elements[index] = elem;

}


array_int  array_int_view(array_int a, int offset )
{

    array_int view = {a.elements + offset, (size_t)((int)a.size - offset)};
    return view;

}




