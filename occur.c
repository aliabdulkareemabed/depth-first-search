#include <stdio.h>

int find_occurrences_count(char* string, char* sub_str)
{
    static int result = 0;
    static int i = 0;

    char c;
    while( (c = string[i]) != 0 && c != sub_str[0]) 
    {
        ++i;
    }

    if(string[i] == 0) 
    {
        int r = result ;
        result = 0;
        return r;
    }
    
    int j = 0;
    while(sub_str[j] != 0 && sub_str[j] == string[i] ) 
    {
        ++i; ++j;
    }
    if(sub_str[j] == 0)
    {
        result += 1;
        int m = i;
        i = 0;
        return find_occurrences_count(&string[m],sub_str);
    }
    else
    {
        int r = result;
        i = 0;
        result = 0;
        return r;
    }
}


int main()
{
    printf("%d\n",find_occurrences_count("abcabc","abc"));
}