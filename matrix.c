#include "matrix.h"

matrix matrix_create(int rows, int cols)
{
	matrix m;
	if(rows < 1 || cols < 1) 
	{
		printf("Error passed dimensions less than 1\n");
		return m;
	}  
	m.entries = array_int_create((size_t)rows * (size_t)cols);

	m.rows = rows;
	m.cols = cols;

	m.row_offset = m.col_offset = 0;
	m.original_cols = cols;
	return m;
}

void matrix_free(matrix* m)
{
	array_int_free(&m->entries);
	m->rows = m->cols = m->original_cols = 0;
}

int matrix_get_elem(matrix* m, int i, int j) 
{
	

	if(i >= m->rows || j>= m->cols) 
	{
		printf("passed an out of range value\n");
		return -1;
	}

	return array_int_get(&m->entries, (m->original_cols * i ) + j);

}

void matrix_set_elem(matrix* m ,int i, int j, int elem)
{

	if(i >= m->rows || j>= m->cols) 
	{
		printf("passed an out of range value\n");
		return;
	}
	array_int_set(&m->entries, (m->original_cols * i ) + j, elem);
}
matrix matrix_get_view(matrix m, int start_x, int start_y, int rows, int cols)
{
	matrix view ;
	if(start_x >= m.cols || start_y >= m.rows) 
	{
		printf("Error invalid start index \n" );
		return view;
	}

	view.entries = array_int_view(m.entries , (m.original_cols*start_x) + start_y);
	view.rows = rows;
	view.cols = cols;
	view.original_cols = m.original_cols;
	return view;
}

void matrix_print(matrix* m)
{
	for(int i = 0 , r = m->rows; i < r;++i)
	{
		for(int j = 0 , c = m->cols; j<c; ++j)
		{

			printf("%d\t", matrix_get_elem(m,i,j));

		}
		printf("\n");
	}
}

matrix matrix_add(matrix m1, matrix m2, matrix result)
{
	int added_value;
	for(int i = 0 , r = m1.rows; i < r;++i)
	{
		for(int j = 0 , c = m1.cols; j<c; ++j)
		{

			added_value = matrix_get_elem(&m1,i,j) + matrix_get_elem(&m2,i,j);
			matrix_set_elem(&result, i,j, added_value);
		}
	
	}
	return result;

}
matrix matrix_subtract(matrix m1, matrix m2, matrix result)
{
	int added_value;
	for(int i = 0 , r = m1.rows; i < r;++i)
	{
		for(int j = 0 , c = m1.cols; j<c; ++j)
		{

			added_value = matrix_get_elem(&m1,i,j) - matrix_get_elem(&m2,i,j);
			matrix_set_elem(&result, i,j, added_value);
		}
	
	}
	return result;

}

void debug_print(matrix* pp,size_t size)
{
	for(int i = 0 ; i < size; ++i)

	{
		matrix_print(&pp[i]);
	}

}

matrix matrix_multiply(matrix m1, matrix m2,matrix result) //assuming NxN dimensions for both matrices and N is a power of 2
{
	if(m1.rows < 2)
	{
	
		int x = matrix_get_elem(&m1, 0,0);
		int y = matrix_get_elem(&m2, 0,0);
		// int old = matrix_get_elem(&result,0,0);
		matrix_set_elem(&result,0,0, (x*y)); //add odd to get the old behaviour
				
		return result;
	}

	int r1 = m1.rows/2;
	int r2 = m1.rows - (m1.rows / 2);

	matrix A = matrix_get_view(m1,0,0, r1, r1);
	matrix E = matrix_get_view(m2,0,0, r1, r1);
	matrix result_1 = matrix_get_view(result,0,0, r1, r1);

	matrix B = matrix_get_view(m1, 0, r1, r1, r2);
	matrix F = matrix_get_view(m2, 0, r1, r1, r2);
	matrix result_2 = matrix_get_view(result, 0, r1, r1, r2);


	matrix C = matrix_get_view(m1, r1, 0, r2, r1);
	matrix G = matrix_get_view(m2, r1, 0, r2, r1);
	matrix result_3 = matrix_get_view(result, r1, 0, r2, r1);


	matrix D = matrix_get_view(m1,r1,r1, r2, r2);
	matrix H = matrix_get_view(m2,r1,r1, r2, r2);
	matrix result_4 = matrix_get_view(result,r1,r1, r2, r2);

	//--------- Normal Recursive Multiplication-------------//
	// matrix_multiply(A,E,result_1); matrix_multiply(B,G,result_1);

	// matrix_multiply(A,F,result_2); matrix_multiply(B,H,result_2);

	// matrix_multiply(C,E,result_3); matrix_multiply(D,G,result_3);

	// matrix_multiply(C,F,result_4); matrix_multiply(D,H,result_4);

	// return result;


	//-----------Strassen Implementation --------------//
	int temp1[16] = {0};
	int temp2[16] = {0};
	int temp3[16] = {0};
	int temp4[16] = {0};
	int temp5[16] = {0};
	int temp6[16] = {0};
	int temp7[16] = {0};
	int temp8[16] = {0};
	int temp9[16] = {0};
	
	
	matrix t1 = {{temp8, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix t2 = {{temp9, 16}, A.rows, A.cols, 0,0,A.original_cols};

	matrix p1 = {{temp1, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix p2 = {{temp2, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix p3 = {{temp3, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix p4 = {{temp4, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix p5 = {{temp5, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix p6 = {{temp6, 16}, A.rows, A.cols, 0,0,A.original_cols};
	matrix p7 = {{temp7, 16}, A.rows, A.cols, 0,0,A.original_cols};


	p1 = matrix_multiply(A, matrix_subtract(F,H,t2), p1);  //P1	

	p2 = matrix_multiply(matrix_add(A,B,t2), H, p2);  //P2

	p3 = matrix_multiply(matrix_add(C,D,t2),E ,p3); // P3

	p4 = matrix_multiply(D,matrix_subtract(G,E,t2),p4); // P4

	p5 = matrix_multiply(matrix_add(A,D,t1),matrix_add(E,H,t2), p5); //P5

	p6 = matrix_multiply(matrix_subtract(B,D,t2), matrix_add(G,H,t1), p6);// P6

	p7 = matrix_multiply(matrix_subtract(A,C,t1), matrix_add(E,F,t2),p7); //P7

	matrix products[7] =
	{
		p1,p2,p3,p4,p5,p6,p7
	};
	
	printf("\n--------- products starts here ---------\n");
	debug_print(products,7);
	printf("\n--------- products ends here ---------\n");

	printf("\n --------- results start here----- \n");

	
	result_1 = matrix_subtract(matrix_add(matrix_add(p5,p4,t1),p6,t2) ,p2,result_1);
	result_2 = matrix_add(p1,p2,result_2);
	result_3 = matrix_add(p3,p4,result_3);
	result_4 = matrix_subtract(matrix_add(p1,p5,t1), matrix_add(p3,p7,t2),result_4);

	matrix results[4] = {result_1,result_2,result_3,result_4};
	debug_print(results,4);
	printf("\n--------- results ends here----- \n");

	return result;


}

int main()
{
	int x = 4;
	int y = 4;
	matrix m = matrix_create(x,y);
	matrix added = matrix_create(x,y);
	for(int i = 0; i < x;++i)
	{
		for(int j = 0; j < y;++j)
		{
			matrix_set_elem(&m , i,  j, (i*y) + j );
			matrix_set_elem(&added , i,  j, 0);
		}
	}

	
	matrix_print(&m);
	printf("\n");
	
	matrix t = matrix_multiply(m,m,added);
	matrix_print(&t);

	matrix_free(&m);


}

